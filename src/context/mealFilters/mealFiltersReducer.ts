import { IngredientId } from '../../types';

export type Location = 'home' | 'delivery';

export const initialState: FiltersState = {
    location: '',
    text: '',
    ingredients: [],
};

export type FiltersState = {
    location: Location | '';
    text: string;
    ingredients: string[];
};

type LocationFilterAction = {
    type: 'SET_LOCATION_FILTER';
    value: Location;
};

type TextFilterAction = {
    type: 'SET_TEXT_FILTER';
    value: string;
};

type IngredientsFilterAction = {
    type: 'SET_INGREDIENTS_FILTER';
    value: IngredientId;
};

type ClearFiltersAction = {
    type: 'CLEAR_FILTERS';
};

export type FiltersStateAction =
    | LocationFilterAction
    | TextFilterAction
    | IngredientsFilterAction
    | ClearFiltersAction;

export const mealFiltersReducer = (
    state: FiltersState,
    action: FiltersStateAction
) => {
    switch (action.type) {
        case 'SET_LOCATION_FILTER':
            return { ...state, location: action.value };
        case 'SET_TEXT_FILTER':
            return { ...state, text: action.value };
        case 'SET_INGREDIENTS_FILTER':
            return {
                ...state,
                ingredients: state.ingredients.concat(action.value),
            };
        case 'CLEAR_FILTERS':
            return initialState;
        default:
            return state;
    }
};
