import styled from 'styled-components';
import { Card as BSCard, Form as BSForm } from 'react-bootstrap';

export const Form = styled(BSForm)`
    min-width: 280px;
    max-width: 330px;
    margin: 0 auto;
`;

export const Card = styled(BSCard)`
    text-align: center;
`;
