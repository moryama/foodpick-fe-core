import styled from 'styled-components';

export const MobileMessageWrapper = styled.div`
    padding: 2em;
    background-color: white;
    margin-top: 5em;
    text-align: center;
    font: Arial;
`;
