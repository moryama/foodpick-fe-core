import { Container } from 'react-bootstrap';
import * as S from './Catcher.styles';

const Catcher = () => {
    return (
        <S.CatcherWrapper fluid>
            <Container>
                <h2>So what&apos;s for dinner?</h2>
                <h1>Pick your next meal in seconds.</h1>
                <S.ContentWrapper>
                    <h3>
                        Step away from decision fatigue, at least food-wise.
                    </h3>
                </S.ContentWrapper>
            </Container>
        </S.CatcherWrapper>
    );
};

export default Catcher;
