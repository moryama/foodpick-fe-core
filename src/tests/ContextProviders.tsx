import {
    MealFiltersContext,
    MealFiltersContextType,
} from '../context/mealFilters/MealFiltersContext';
import { MealsContext, MealsContextType } from '../context/meals/MealsContext';
import {
    NotificationContext,
    NotificationContextType,
} from '../context/notification/NotificationContext';
import { UserContext, UserContextType } from '../context/UserContext';

// eslint-disable-next-line @typescript-eslint/no-empty-function
export const defaultDispatch = () => {};
const emptyString = '';

const mealsDefaultContextValue: MealsContextType = {
    meals: [
        {
            name: 'one meal',
            user: 'user',
            id: 'id',
        },
    ],
    dispatch: defaultDispatch,
    isLoading: false,
};

const mealFiltersDefaultContextValue: MealFiltersContextType = {
    filters: {
        location: emptyString,
        text: emptyString,
        ingredients: [],
    },
    dispatch: defaultDispatch,
};

const userDefaultContextValue: UserContextType = {
    user: { token: emptyString, username: emptyString },
    dispatch: defaultDispatch,
};

const notificationDefaultContextValue: NotificationContextType = {
    setNotification: defaultDispatch,
    notificationComponent: () => undefined,
};

type ContextProvidersProps = {
    children: React.ReactNode;
    mealsContextValue?: MealsContextType;
};

export const ContextProviders = ({
    children,
    mealsContextValue,
}: ContextProvidersProps) => {
    return (
        <UserContext.Provider value={userDefaultContextValue}>
            <NotificationContext.Provider
                value={notificationDefaultContextValue}
            >
                <MealsContext.Provider
                    value={mealsContextValue || mealsDefaultContextValue}
                >
                    <MealFiltersContext.Provider
                        value={mealFiltersDefaultContextValue}
                    >
                        {children}
                    </MealFiltersContext.Provider>
                </MealsContext.Provider>
            </NotificationContext.Provider>
        </UserContext.Provider>
    );
};
