import { Jumbotron } from 'react-bootstrap';
import styled from 'styled-components';
import { COLORS } from '../../constants';

export const CatcherWrapper = styled(Jumbotron)`
    background-color: ${COLORS.ABOUT};
    border-radius: 10px;
    padding: 2em;
`;

export const ContentWrapper = styled.div`
    margin-top: 2.5em;
`;
