import '@testing-library/jest-dom/extend-expect';
import { render, fireEvent, RenderResult } from '@testing-library/react';
import LoginView from './LoginView';

describe.skip('<Unibar />', () => {
    let component: RenderResult;
    let mockOnSubmit: jest.MockedFunction<any>;

    beforeEach(() => {
        mockOnSubmit = jest.fn();

        component = render(<LoginView />);
    });

    test('when clicking submit once, it calls the related handler function with the expected data', () => {
        const username = component.container.querySelector(
            '[data-test-id=username]'
        );
        const password = component.container.querySelector(
            '[data-test-id=password]'
        );
        const form = component.container.querySelector('form');

        username &&
            fireEvent.change(username, {
                target: { value: 'username' },
            });
        password &&
            fireEvent.change(password, {
                target: { value: 'password' },
            });
        form && fireEvent.submit(form);

        expect(mockOnSubmit.mock.calls).toHaveLength(1);
        expect(mockOnSubmit.mock.calls[0][0].username).toBe('username');
        expect(mockOnSubmit.mock.calls[0][0].password).toBe('password');
    });
});
