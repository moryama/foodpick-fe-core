import styled, { createGlobalStyle } from 'styled-components';
import background from './images/background.jpg';

export const GlobalStyle = createGlobalStyle`
    body {
        font-family: 'Quicksand', sans-serif;

        button.btn {
            margin: 0.3em;
        }

        h1 {
            font-size: 4em;
        }

        h2 {
            font-size: 3em;
        }

        h3 {
            font-weight: 300;
        }
    }
`;

export const AppWrapper = styled.div`
    background-image: url(${background});
    background-repeat: repeat;

    /* Fill whole space on screen */
    width: 100%;
    height: 100%;
    margin: 0px;
    padding: 0px;
    overflow-x: hidden;

    /* Make the footer stick */
    display: flex;
    min-height: 100vh;
    flex-direction: column;
`;

export const ContentWrapper = styled.div`
    /* Make the footer stick */
    flex: 1;
`;
