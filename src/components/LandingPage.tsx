import { Container, Row } from 'react-bootstrap';
import { useLoginFormContext } from '../context/LoginFormContext';
import About from './About/About';
import LoginView from './LoginView/LoginView';

export const LandingPage = () => {
    const { isLoginFormVisible } = useLoginFormContext();

    return (
        <Container>
            <Row className="justify-content-md-center">
                {isLoginFormVisible ? <LoginView /> : <About />}
            </Row>
        </Container>
    );
};
