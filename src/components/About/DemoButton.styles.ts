import { Button as BSButton } from 'react-bootstrap';
import styled from 'styled-components';
import { COLORS } from '../../constants';

export const Button = styled(BSButton)`
    background-color: ${COLORS.DEMO};
    border: ${COLORS.DEMO};
`;

export const DemoButtonWrapper = styled.div`
    text-align: center;
`;
