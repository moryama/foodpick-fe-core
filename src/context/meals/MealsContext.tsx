import { MealType } from '../../types';
import { createContext } from '../createContext';
import mealService from '../../services/meals';
import React, { useEffect, useReducer, useState } from 'react';
import { initialState, MealsAction, mealsReducer } from './mealsReducer';

export type MealsContextType = {
    meals: MealType[];
    dispatch: React.Dispatch<MealsAction>;
    isLoading: boolean;
};

export const [useMealsContext, MealsContext] =
    createContext<MealsContextType>();
MealsContext.displayName = 'MealsContext';

export const MealsContextProvider = ({
    children,
}: {
    children: React.ReactNode;
}) => {
    const [isLoading, setIsLoading] = useState(true);
    const [meals, dispatch] = useReducer(mealsReducer, initialState);

    useEffect(() => {
        const fetchMeals = async () => {
            const fetchedMeals = await mealService.getAll();
            dispatch({ type: 'INIT_MEALS', meals: fetchedMeals });
            setIsLoading(false);
        };

        fetchMeals();
    }, []);

    return (
        <MealsContext.Provider value={{ meals, dispatch, isLoading }}>
            {children}
        </MealsContext.Provider>
    );
};
