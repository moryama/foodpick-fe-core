export const isInputObjectEmpty = (inputObject: Record<string, unknown>) => {
    for (const _item in inputObject) return false;
    return true;
};
