import { Instructions } from '../Instructions';

import * as S from './HowItWorks.styles';

const HowItWorks = () => {
    return (
        <S.HowItWorksWrapper fluid>
            <h3>HOW IT WORKS</h3>
            <Instructions />
        </S.HowItWorksWrapper>
    );
};

export default HowItWorks;
