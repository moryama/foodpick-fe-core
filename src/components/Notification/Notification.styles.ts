import { Alert as BSAlert, Container } from 'react-bootstrap';
import styled from 'styled-components';

export const NotificationWrapper = styled(Container)`
    margin-top: 30px;
`;

export const Alert = styled(BSAlert)`
    border-radius: 8px;
`;
