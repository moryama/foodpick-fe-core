import { Location } from './context/mealFilters/mealFiltersReducer';

export const LABELS = {
    MEAL: 'meal',
    TIME: 'time to table',
    TIMESPEC: 'min',
    HOME: '🏠',
    DELIVERY: '🚴🏼',
    INGREDIENTS: 'ingredients',
    EMPTYMEALLIST: 'Nothing here',
    BUTTONS: {
        ADD: 'add new',
        REMOVE: 'delete',
        SAVE: 'save',
        CLEAR: 'clear filters',
        FILTERS: 'filters',
        LOGIN: 'Sign in',
        LOGOUT: 'Sign out',
        DEMO: 'TRY DEMO',
    },
    PLACEHOLDERS: {
        INGREDIENTS: 'type ingredient + ENTER',
        UNIBAR: 'search food ideas or add a new one',
    },
};

export const TIMETOTABLEVALUES = [10, 20, 30, 45, 60, 90, 120, 150, 180];

const PALETTE = {
    BLUE: '#61738B',
    GREY: '#B4B7BD',
    RED: '#ea5455',
    ORANGE: '#f07b3f',
    YELLOW: '#FFE7AA',
    WHITE: '#FFFFFF',
};

export const COLORS = {
    NAVBAR: `${PALETTE.WHITE}`,
    ADD: `${PALETTE.BLUE}`,
    FILTERPANEL: `${PALETTE.GREY}`,
    FILTER: `${PALETTE.BLUE}`,
    CLEARFILTERS: `${PALETTE.BLUE}`,
    INGREDIENTS: `${PALETTE.BLUE}`,
    MEAL: `${PALETTE.YELLOW}`,
    USER: `${PALETTE.ORANGE}`,
    WHITE: `${PALETTE.WHITE}`,
    ABOUT: `${PALETTE.YELLOW}`,
    DEMO: `${PALETTE.ORANGE}`,
    FOOTER: `${PALETTE.GREY}`,
    SELECTEDFILTER: `${PALETTE.YELLOW}`,
};

const LOCATIONS = {
    home: 'home' as Location,
    delivery: 'delivery' as Location,
};

export const HOME = LOCATIONS.home;
export const DELIVERY = LOCATIONS.delivery;

export const UNKNOWN_ERROR_MESSAGE = 'Some unknown error occurred';
