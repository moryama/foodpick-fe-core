export type Ingredients = IngredientType[];

export type IngredientId = string;

export type IngredientType = {
    id: IngredientId;
    name: string;
};

export type MealId = string;

export type MealType = {
    ingredients?: Ingredients;
    name: string;
    home?: boolean;
    time?: number;
    user: string;
    id: MealId;
};
