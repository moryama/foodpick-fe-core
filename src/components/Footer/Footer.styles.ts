import styled from 'styled-components';
import { COLORS } from '../../constants';

export const Footer = styled.footer`
    background-color: ${COLORS.FOOTER};
    text-align: center;
    height: 5em;
    opacity: 80%;

    a {
        color: #212529;
    }

    a:hover {
        color: #414950;
    }
`;

export const ContentWrapper = styled.div`
    position: relative;
    top: 50%;
    transform: translateY(-50%);
    font-size: 1.2em;
`;
