import { Nav } from 'react-bootstrap';
import { useUserContext } from '../../context/UserContext';
import * as S from './NavbarMenu.styles';
import { GoToLoginButton } from './GoToLoginButton';
import { UserActionDropdown } from './UserActionDropdown';
import { HowItWorksDropdown } from './HowItWorksDropdown';
import { Logo } from './Logo';

export const NavbarMenu = () => {
    const { user } = useUserContext();

    return (
        <S.Navbar>
            <Logo />
            <Nav className="ml-auto">
                {user ? (
                    <>
                        <HowItWorksDropdown />
                        <UserActionDropdown user={user} />
                    </>
                ) : (
                    <GoToLoginButton />
                )}
            </Nav>
        </S.Navbar>
    );
};
