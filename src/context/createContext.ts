import {
    createContext as reactCreateContext,
    useContext as reactUseContext,
} from 'react';

export function createContext<O extends Record<string, unknown> | null>() {
    const context = reactCreateContext<O | undefined>(undefined);

    const useContext = () => {
        const c = reactUseContext(context);

        if (c === undefined) {
            throw new Error('You must pass some value to the context provider');
        }
        return c;
    };

    return [useContext, context] as const;
}
