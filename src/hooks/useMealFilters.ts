import { MealType } from '../types';
import { filterMealsByIngredients } from '../components/MealsView/filterMealsByIngredient';
import { filterMealsByText } from '../components/MealsView/filterMealsByText';
import { DELIVERY, HOME } from '../constants';
import { useMealFiltersContext } from '../context/mealFilters/MealFiltersContext';

export const useMealFilters = (meals: MealType[]) => {
    let filteredMeals = meals;

    const { filters } = useMealFiltersContext();
    const locationFilter = filters.location;
    const unibarTextFilter = filters.text;
    const ingredientsFilter = filters.ingredients;

    if (unibarTextFilter) {
        filteredMeals = filterMealsByText(filteredMeals, unibarTextFilter);
    }

    if (ingredientsFilter.length > 0) {
        filteredMeals = filterMealsByIngredients(
            filteredMeals,
            ingredientsFilter
        );
    }

    if (locationFilter === HOME) {
        filteredMeals = filteredMeals.filter((meal) => meal.home);
    } else if (locationFilter === DELIVERY) {
        filteredMeals = filteredMeals.filter((meal) => !meal.home);
    }

    return filteredMeals;
};
