import styled from 'styled-components';

export const IngredientsFormWrapper = styled.div`
    margin-top: 1em;
    width: 100%;
`;
