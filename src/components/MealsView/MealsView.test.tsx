import { render } from '@testing-library/react';
import {
    ContextProviders,
    defaultDispatch,
} from '../../tests/ContextProviders';
import MealsView from './MealsView';
// https://stackoverflow.com/a/65726104/13628287
import '@testing-library/jest-dom/extend-expect';
import { LABELS } from '../../constants';
import { MealsContextType } from '../../context/meals/MealsContext';

const getMealsViewComponent = (contextValue: MealsContextType) => {
    return render(
        <ContextProviders mealsContextValue={contextValue}>
            <MealsView />
        </ContextProviders>
    );
};

describe('<MealsView />', () => {
    it('should render one meal', () => {
        const contextValue = {
            meals: [
                {
                    name: 'one meal',
                    user: 'user',
                    id: 'id',
                },
            ],
            dispatch: defaultDispatch,
            isLoading: false,
        };

        const component = getMealsViewComponent(contextValue);

        expect(component.container).toHaveTextContent('one meal');
    });

    it('should render multiple meals', () => {
        const contextValue = {
            meals: [
                {
                    name: 'one meal',
                    user: 'user',
                    id: '1',
                },
                {
                    name: 'another meal',
                    user: 'user',
                    id: '2',
                },
            ],
            dispatch: defaultDispatch,
            isLoading: false,
        };

        const component = getMealsViewComponent(contextValue);

        expect(component.container).toHaveTextContent('one meal');
        expect(component.container).toHaveTextContent('another meal');
    });

    it('should render a message if no meal is saved', () => {
        const contextValue = {
            meals: [],
            dispatch: defaultDispatch,
            isLoading: false,
        };

        const component = getMealsViewComponent(contextValue);

        expect(component.container).toHaveTextContent(LABELS.EMPTYMEALLIST);
    });
});
