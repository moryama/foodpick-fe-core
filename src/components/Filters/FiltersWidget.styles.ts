import styled from 'styled-components';
import { COLORS } from '../../constants';

export const FiltersWidgetWrapper = styled.div`
    width: 50%;
    margin: 0 auto;
    padding: 1em;
    border-radius: 4px;
    background-color: ${COLORS.FILTERPANEL};
`;
