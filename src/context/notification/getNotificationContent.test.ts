import { UNKNOWN_ERROR_MESSAGE } from '../../constants';
import { getNotificationContent } from './getNotificationContent';
import { NotificationData } from './NotificationContext';
import { NotificationLabel, notifications } from './notifications';

// TODO: finish this and make a couple more (?)
describe(getNotificationContent.name, () => {
    it('should return notification content', () => {
        const label = 'LOGIN_SUCCESS' as NotificationLabel;
        const input = { label };
        const expectedOutput = {
            type: notifications[label].type,
            title: notifications[label].title,
            message: notifications[label].message,
        };

        const output = getNotificationContent(input);

        expect(output).toEqual(expectedOutput);
    });

    it('should return notification content with error message', () => {
        const label = 'LOGIN_FAIL' as NotificationLabel;
        const input = {
            label,
            error: { cause: 'some error', name: '', message: '' },
        };
        const expectedOutput = {
            type: notifications[label].type,
            title: notifications[label].title,
            message: UNKNOWN_ERROR_MESSAGE,
        };

        const output = getNotificationContent(input);

        expect(output).toEqual(expectedOutput);
    });

    it('should return undefined if empty object is passed', () => {
        const input = {} as NotificationData;
        const output = getNotificationContent(input);

        expect(output).toBe(undefined);
    });
});
