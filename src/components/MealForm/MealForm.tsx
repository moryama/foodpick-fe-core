import { ChangeEvent, FormEvent, KeyboardEvent, useState } from 'react';
import Card from 'react-bootstrap/Card';
import LocationFormButton from './LocationFormButton';
import TimeForm from './TimeForm';
import IngredientsForm from './IngredientsForm';
import { userPressedEnter } from '../../helpers/userPressedEnter';
import SaveButton from './SaveButton';
import { MealType } from '../../types';
import { DELIVERY, HOME } from '../../constants';
import { useMealsContext } from '../../context/meals/MealsContext';
import { addMeal } from '../../context/meals/mealsReducer';
import { useMealFiltersContext } from '../../context/mealFilters/MealFiltersContext';
import { useNotificationContext } from '../../context/notification/NotificationContext';
import * as S from './MealForm.styles';
import { useToggleVisibility } from '../../hooks/useToggleVisibility';

export type MealInputType = {
    ingredients?: IngredientName[];
    name: string;
    home?: boolean;
    time: number | undefined;
};

type IngredientName = string;

const MealForm = () => {
    const [isHomeMade, setIsHomeMade] = useState(true);
    const [time, setTime] = useState<number | undefined>(undefined);
    const [ingredients, setIngredients] = useState<IngredientName[]>([]);

    const { meals, dispatch: mealsDispatch } = useMealsContext();
    const { filters, dispatch: mealFiltersDispatch } = useMealFiltersContext();
    const { setNotification } = useNotificationContext();
    const { setIsMealFormVisible } = useToggleVisibility();

    const name = filters.text;

    const mealHasName = name !== '';

    const createNew = (event: FormEvent) => {
        event.preventDefault();
        const isDuplicated = meals.find((meal: MealType) => meal.name === name);

        if (isDuplicated) {
            if (!window.confirm(`You already have '${name}'. Add anyway?`)) {
                return;
            }
        }

        const mealObject: MealInputType = {
            name: name || '',
            time,
            home: isHomeMade,
            ingredients,
        };

        addMeal(mealsDispatch, mealObject, setNotification);
        setIsMealFormVisible(false);
        mealFiltersDispatch({ type: 'CLEAR_FILTERS' });
    };

    const handleTimeForm = (event: ChangeEvent<HTMLInputElement>) => {
        event.preventDefault();
        setTime(Number(event.target.value));
    };

    const handleOnKeyPress = (event: KeyboardEvent<HTMLInputElement>) => {
        const target = event.target as HTMLInputElement;
        const ingredient = target.value;

        if (userPressedEnter(event) && ingredient) {
            setIngredients(ingredients.concat(ingredient));
            target.value = '';
        }
    };

    const preventSaveFormOnEnter = (e: KeyboardEvent<HTMLInputElement>) =>
        userPressedEnter(e) ? e.preventDefault() : null;

    return (
        <div>
            <S.Form
                onSubmit={createNew}
                onKeyDown={preventSaveFormOnEnter}
                autoComplete="off"
            >
                <S.Card data-test-id="meal-form" bg="light">
                    <Card.Body>
                        <Card.Title>{name}</Card.Title>
                        <LocationFormButton
                            location={HOME}
                            onClick={() => setIsHomeMade(true)}
                            isSelected={isHomeMade}
                        />
                        <LocationFormButton
                            location={DELIVERY}
                            onClick={() => setIsHomeMade(false)}
                            isSelected={!isHomeMade}
                        />
                        <TimeForm onChange={(e) => handleTimeForm(e)} />
                        {isHomeMade && (
                            <IngredientsForm
                                handleOnKeyPress={handleOnKeyPress}
                                ingredients={ingredients}
                            />
                        )}
                    </Card.Body>
                    <SaveButton isEnabled={mealHasName} />
                </S.Card>
            </S.Form>
        </div>
    );
};

export default MealForm;
