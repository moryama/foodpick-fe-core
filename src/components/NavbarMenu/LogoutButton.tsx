import { LABELS } from '../../constants';
import * as S from './UserActionDropdown.styles';
import { useLogout } from '../../hooks/useLogout';

export const LogoutButton = () => {
    const { logout } = useLogout();

    return (
        <S.DropdownItem
            data-test-id="nav-logout"
            eventKey="logout"
            onSelect={() => logout()}
        >
            {LABELS.BUTTONS.LOGOUT}
        </S.DropdownItem>
    );
};
