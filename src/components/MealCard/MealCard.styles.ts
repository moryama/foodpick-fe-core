import styled from 'styled-components';
import { Card as BSCard } from 'react-bootstrap';
import { COLORS } from '../../constants';

export const Card = styled(BSCard)`
    min-width: 260px;
    border: none;
    border-radius: 4px;
    text-align: center;
    margin-bottom: 20px;
`;

export const CardFooter = styled(BSCard.Footer)`
    background-color: ${COLORS.FOOTER};
    opacity: 80%;
`;

export const CardBody = styled(Card.Body)`
    background-color: ${COLORS.MEAL};
`;

export const CardHeader = styled(Card.Header)`
    background-color: ${COLORS.WHITE};
`;
