import { Nav } from 'react-bootstrap';
import { LABELS } from '../../constants';
import { useLoginFormContext } from '../../context/LoginFormContext';

export const GoToLoginButton = () => {
    const { setIsLoginFormVisible } = useLoginFormContext();

    return (
        <Nav.Link eventKey="login" onSelect={() => setIsLoginFormVisible(true)}>
            {LABELS.BUTTONS.LOGIN}
        </Nav.Link>
    );
};
