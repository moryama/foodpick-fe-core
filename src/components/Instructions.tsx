import { DELIVERY, HOME } from '../constants';
import { Icon } from './Icon';

export const Instructions = () => {
    return (
        <>
            Save and search your food ideas. Quick and easy.
            <ul style={{ marginTop: '20px' }}>
                <li>
                    <Icon type={HOME} size="md" /> you cook it yourself
                </li>
                <li>
                    <Icon type={DELIVERY} size="md" /> you get it with delivery
                </li>
                <li>
                    <Icon type="time" size="md" /> how long until it&apos;s on
                    your table
                </li>
                <li>Ingredients: what you need to cook it</li>
            </ul>
            Use filters to pick the right food idea for today.
        </>
    );
};
