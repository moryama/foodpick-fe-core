import { KeyboardEventHandler } from 'react';
import { Form } from 'react-bootstrap';
import { LABELS } from '../../constants';
import Tag from '../Tag/Tag';
import * as S from './IngredientsForm.styles';

type IngredientsFormType = {
    handleOnKeyPress: KeyboardEventHandler<HTMLInputElement>;
    ingredients: string[];
};

const IngredientsForm = ({
    handleOnKeyPress,
    ingredients,
}: IngredientsFormType) => {
    const returnIngredientTags = () => {
        return ingredients.map((ingredient) => (
            <Tag key={ingredient} text={ingredient} isEnabled={false} />
        ));
    };

    return (
        <S.IngredientsFormWrapper>
            <Form.Label>{LABELS.INGREDIENTS}</Form.Label>
            <Form.Control
                data-test-id="ingredients"
                onKeyDown={handleOnKeyPress}
                placeholder={LABELS.PLACEHOLDERS.INGREDIENTS}
                aria-label="type and ingredient and press enter"
            />
            <br />
            {returnIngredientTags()}
        </S.IngredientsFormWrapper>
    );
};

export default IngredientsForm;
