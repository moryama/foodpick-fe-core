import 'bootstrap/dist/css/bootstrap.min.css';
import Footer from './components/Footer/Footer';
import { UserToken } from './services/login';
import { NavbarMenu } from './components/NavbarMenu/NavbarMenu';
import { UserPage } from './components/UserPage';
import { LandingPage } from './components/LandingPage';
import { LoginFormContextProvider } from './context/LoginFormContext';
import { useNotificationContext } from './context/notification/NotificationContext';
import * as S from './App.styles';
import { useSetCurrentUser } from './hooks/useSetCurrentUser';

export type LoggedInUser = {
    token: UserToken;
    username: string;
};

const App = () => {
    const { user } = useSetCurrentUser();
    const { notificationComponent } = useNotificationContext();

    return (
        <>
            <S.GlobalStyle />
            <S.AppWrapper>
                <S.ContentWrapper>
                    <LoginFormContextProvider>
                        <>
                            <NavbarMenu />
                            {notificationComponent()}
                            {user ? null : <LandingPage />}
                        </>
                    </LoginFormContextProvider>
                    {user ? <UserPage /> : null}
                </S.ContentWrapper>
                <Footer />
            </S.AppWrapper>
        </>
    );
};

export default App;
