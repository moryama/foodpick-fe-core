import { IngredientId, Ingredients, MealId, MealType } from '../../types';
import mealService, { CreateMealAPIResponse } from '../../services/meals';
import ingredientService from '../../services/ingredients';
import React from 'react';
import { MealInputType } from '../../components/MealForm/MealForm';
import { SetNotificationType } from '../notification/NotificationContext';

type InitMealsAction = {
    type: 'INIT_MEALS';
    meals: MealType[];
};

type AddMealAction = {
    type: 'ADD_MEAL';
    meal: MealType;
};

type DeleteMealAction = {
    type: 'DELETE_MEAL';
    mealId: MealId;
};

export type MealsAction = InitMealsAction | AddMealAction | DeleteMealAction;

export const initialState: MealType[] = [];

export const mealsReducer = (state = initialState, action: MealsAction) => {
    switch (action.type) {
        case 'INIT_MEALS':
            return action.meals;
        case 'ADD_MEAL':
            return state.concat(action.meal);
        case 'DELETE_MEAL':
            return state.filter((meal) => meal.id !== action.mealId);
        default:
            return state;
    }
};

const getMealWithIngredients = async (fetchedMeal: CreateMealAPIResponse) => {
    let ingredientsArray: Ingredients = [];
    if (fetchedMeal.ingredients) {
        const promiseToGetIngredientsNames = fetchedMeal.ingredients.map(
            (i: IngredientId) => ingredientService.getById(i)
        );
        ingredientsArray = await Promise.all(promiseToGetIngredientsNames);
    }
    const mealWithIngredients = {
        ...fetchedMeal,
        ingredients: ingredientsArray,
    };

    return mealWithIngredients;
};

export const addMeal = async (
    dispatch: React.Dispatch<MealsAction>,
    mealObject: MealInputType,
    setNotification: SetNotificationType
) => {
    try {
        const fetchedMeal = await mealService.create(mealObject);
        const mealHasIngredients =
            fetchedMeal.ingredients && fetchedMeal.ingredients.length > 0;

        let mealToDispatch;

        if (mealHasIngredients) {
            // we want the ingredients names immediately available for filter
            mealToDispatch = await getMealWithIngredients(fetchedMeal);
        } else {
            delete fetchedMeal.ingredients;
            mealToDispatch = fetchedMeal as unknown as MealType;
        }
        dispatch({ type: 'ADD_MEAL', meal: mealToDispatch });
        setNotification({ label: 'ADD_MEAL_SUCCESS' });
    } catch (error) {
        setNotification({ label: 'ADD_MEAL_FAIL', error: error });
    }
};

export const deleteMeal = async (
    dispatch: React.Dispatch<MealsAction>,
    mealId: MealId,
    setNotification: SetNotificationType
) => {
    try {
        await mealService.drop(mealId);
        dispatch({ type: 'DELETE_MEAL', mealId: mealId });
        setNotification({ label: 'DELETE_MEAL_SUCCESS' });
    } catch (error) {
        setNotification({ label: 'DELETE_MEAL_FAIL', error: error });
    }
};
