import styled from 'styled-components';
import { Dropdown } from 'react-bootstrap';
import { COLORS } from '../../constants';

export const DropdownItem = styled(Dropdown.Item)`
    background-color: ${COLORS.USER};
`;
