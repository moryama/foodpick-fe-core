import { LABELS } from '../../constants';
import { useMealFiltersContext } from '../../context/mealFilters/MealFiltersContext';
import * as S from './ClearFiltersButton.styles';

export const ClearFiltersButton = () => {
    const { dispatch } = useMealFiltersContext();

    return (
        <S.Button
            type="button"
            data-test-id="clear-filters-button"
            onClick={() => dispatch({ type: 'CLEAR_FILTERS' })}
            aria-label="clear all filters"
        >
            {LABELS.BUTTONS.CLEAR}
        </S.Button>
    );
};
