import React, { useState } from 'react';
import { Notification } from '../../components/Notification/Notification';
import { createContext } from '../createContext';
import { getNotificationContent } from './getNotificationContent';
import { NotificationLabel } from './notifications';

export type SetNotificationType = React.Dispatch<
    React.SetStateAction<NotificationData | undefined>
>;

export type NotificationContextType = {
    setNotification: SetNotificationType;
    notificationComponent: () => JSX.Element | undefined;
};
export type NotificationData = {
    label: NotificationLabel;
    error?: unknown;
};

export const [useNotificationContext, NotificationContext] =
    createContext<NotificationContextType>();
NotificationContext.displayName = 'NotificationContext';

export const NotificationContextProvider = ({
    children,
}: {
    children: React.ReactNode;
}) => {
    const [notificationData, setNotificationData] = useState<
        NotificationData | undefined
    >();

    const notificationContent =
        notificationData && getNotificationContent(notificationData);

    const component = () => {
        return (
            notificationContent && (
                <Notification content={notificationContent} />
            )
        );
    };

    return (
        <NotificationContext.Provider
            value={{
                setNotification: setNotificationData,
                notificationComponent: component,
            }}
        >
            {children}
        </NotificationContext.Provider>
    );
};
