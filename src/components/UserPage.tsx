import { Container } from 'react-bootstrap';
import { MealFiltersContextProvider } from '../context/mealFilters/MealFiltersContext';
import { MealFormContextProvider } from '../context/MealFormContext';
import { MealsContextProvider } from '../context/meals/MealsContext';
import MealsView from './MealsView/MealsView';
import Unibar from './Unibar/Unibar';

export const UserPage = () => {
    return (
        <MealsContextProvider>
            <MealFiltersContextProvider>
                <Container data-test-id="user-page">
                    <MealFormContextProvider>
                        <Unibar />
                    </MealFormContextProvider>
                    <MealsView />
                </Container>
            </MealFiltersContextProvider>
        </MealsContextProvider>
    );
};
