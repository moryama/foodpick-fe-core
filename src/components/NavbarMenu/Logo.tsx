import logo from '../../images/logo.jpg';
import * as S from './Logo.styles';

export const Logo = () => {
    return (
        <S.NavbarBrand>
            <S.BrandImage src={logo} alt="logo" />
        </S.NavbarBrand>
    );
};
