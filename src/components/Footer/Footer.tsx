import { getCurrentYear } from '../../helpers/getCurrentYear';
import * as S from './Footer.styles';

const Footer = () => {
    const currYear = getCurrentYear();
    return (
        <S.Footer>
            <S.ContentWrapper>
                moryama {currYear}
                <span> / </span>
                <a href="mailto:5599992-moryama@users.noreply.gitlab.com">
                    Contact me
                </a>
                <span> / </span>
                <a href="https://gitlab.com/moryama/foodpick">code on GitLab</a>
            </S.ContentWrapper>
        </S.Footer>
    );
};

export default Footer;
