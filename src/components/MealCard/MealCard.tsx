import RemoveButton from './RemoveButton';
import * as S from './MealCard.styles';
import { MealInfo } from './MealInfo';
import { MealType } from '../../types';

type MealProps = {
    meal: MealType;
};

const MealCard = ({ meal }: MealProps) => {
    return (
        <S.Card bg="light" data-test-id="meal-card">
            <S.CardBody>
                <S.CardHeader>{meal.name}</S.CardHeader>
                <MealInfo meal={meal} />
            </S.CardBody>
            <S.CardFooter>
                <RemoveButton mealId={meal.id} mealName={meal.name} />
            </S.CardFooter>
        </S.Card>
    );
};

export default MealCard;
