import { Ingredients } from '../types';

export const sortArrayAlphabetically = (array: Ingredients) => {
    const sortedArray = array.sort(function (a, b) {
        const nameA = a.name.toUpperCase();
        const nameB = b.name.toUpperCase();
        return nameA < nameB ? -1 : nameA > nameB ? 1 : 0;
    });
    return sortedArray;
};
