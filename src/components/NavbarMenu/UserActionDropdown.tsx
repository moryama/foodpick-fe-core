import { NavDropdown } from 'react-bootstrap';
import { LoggedInUser } from '../../App';
import { LogoutButton } from './LogoutButton';

type UserActionDropdownProps = {
    user: LoggedInUser;
};

export const UserActionDropdown = ({ user }: UserActionDropdownProps) => {
    return (
        <NavDropdown
            data-test-id="nav-dropdown-user"
            title={`Hi ${user.username}`}
            id="login-dropdown"
        >
            <LogoutButton />
        </NavDropdown>
    );
};
