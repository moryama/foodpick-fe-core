import { FormEvent, useState } from 'react';
import { Col, Form, Row } from 'react-bootstrap';
import { LABELS } from '../../constants';
import { LoginCredentials } from './LoginView';
import { useUserContext } from '../../context/UserContext';
import loginService from '../../services/login';
import mealService from '../../services/meals';
import { useLoginFormContext } from '../../context/LoginFormContext';
import { useNotificationContext } from '../../context/notification/NotificationContext';
import * as S from './LoginForm.styles';

export const LoginForm = () => {
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');

    const { dispatch } = useUserContext();
    const { setIsLoginFormVisible } = useLoginFormContext();
    const { setNotification } = useNotificationContext();

    const handleLogin = async (credentials: LoginCredentials) => {
        let fetchedUser;
        try {
            fetchedUser = await loginService.login(credentials);
        } catch (error) {
            setNotification({ label: 'LOGIN_FAIL', error: error });
        } finally {
            setUsername('');
            setPassword('');
        }

        if (fetchedUser) {
            mealService.setToken(fetchedUser.token);
            dispatch({ type: 'SET_USER', user: fetchedUser });
            localStorage.setItem('token', fetchedUser.token);
            localStorage.setItem('username', fetchedUser.username);
            setNotification({ label: 'LOGIN_SUCCESS' });
            setIsLoginFormVisible(false);
        }
    };

    const loginUser = (event: FormEvent<HTMLFormElement>) => {
        event.preventDefault();

        handleLogin({
            username,
            password,
        });
    };

    return (
        <Form onSubmit={(e) => loginUser(e)}>
            <Form.Group as={Row}>
                <Form.Label column sm={2}>
                    username
                </Form.Label>
                <Col sm={10}>
                    <Form.Control
                        type="text"
                        data-test-id="username"
                        placeholder="username"
                        aria-label="username"
                        required
                        onChange={({ target }) => setUsername(target.value)}
                        autoFocus
                    />
                </Col>
            </Form.Group>
            <Form.Group as={Row}>
                <Form.Label column sm={2}>
                    password
                </Form.Label>
                <Col sm={10}>
                    <Form.Control
                        type="password"
                        data-test-id="password"
                        placeholder="password"
                        aria-label="password"
                        required
                        autoComplete="off"
                        onChange={({ target }) => setPassword(target.value)}
                    />
                </Col>
            </Form.Group>
            <S.Button data-test-id="login-button" type="submit">
                {LABELS.BUTTONS.LOGIN}
            </S.Button>
        </Form>
    );
};
