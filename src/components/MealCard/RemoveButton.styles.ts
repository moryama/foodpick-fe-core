import styled from 'styled-components';
import { Button as BsButton } from 'react-bootstrap';

export const Button = styled(BsButton)`
    background: none;
    border: none;
`;
