import { useCallback, useEffect, useState } from 'react';
import { Alert, Button } from 'react-bootstrap';
import { useNotificationContext } from '../../context/notification/NotificationContext';
import { NotificationContent } from '../../context/notification/notifications';
import * as S from './Notification.styles';

type NotificationProps = {
    content: NotificationContent;
};

export const Notification = ({ content }: NotificationProps) => {
    const [countdown, setCountdown] = useState(5);

    const { setNotification } = useNotificationContext();

    const closeNotification = useCallback(() => {
        setNotification(undefined);
    }, [setNotification]);

    useEffect(() => {
        const scrollToTop = () => {
            return window.scrollTo(0, 0);
        };
        const autoClose = () => {
            return setTimeout(() => closeNotification(), 5000);
        };
        scrollToTop();
        autoClose();

        const cleanUp = () => {
            clearTimeout(autoClose());
        };
        return cleanUp;
    }, [closeNotification]);

    useEffect(() => {
        setTimeout(() => setCountdown(countdown - 1), 1000);
    }, [countdown]);

    return (
        <S.NotificationWrapper>
            <S.Alert variant={content.type}>
                <Alert.Heading>{content.title}</Alert.Heading>
                <p>{content.message}</p>
                <div className="d-flex justify-content-end">
                    <Button
                        onClick={() => closeNotification()}
                        variant={`outline-${content.type}`}
                    >
                        Close in {countdown}
                    </Button>
                </div>
            </S.Alert>
        </S.NotificationWrapper>
    );
};
