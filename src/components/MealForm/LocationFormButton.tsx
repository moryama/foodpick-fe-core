import { Location } from '../../context/mealFilters/mealFiltersReducer';
import { LocationButton } from '../LocationButton';

type LocationButtonProps = {
    isSelected: boolean;
    location: Location;
    onClick: (location: string) => void;
};

const LocationFormButton = ({
    isSelected,
    location,
    onClick,
}: LocationButtonProps) => {
    return (
        <LocationButton
            buttonType="form"
            data-test-id={`${location}-button`}
            isSelected={isSelected}
            locationType={location}
            onClick={() => {
                onClick(location);
            }}
            value={location}
        />
    );
};

export default LocationFormButton;
