import { LABELS } from '../../constants';
import * as S from './FiltersWidgetButton.styles';

type FiltersWidgetButtonProps = {
    onClick: () => void;
};

export const FiltersWidgetButton = ({ onClick }: FiltersWidgetButtonProps) => {
    return (
        <S.Button onClick={onClick}>{LABELS.BUTTONS.FILTERS}</S.Button>
    );
};
