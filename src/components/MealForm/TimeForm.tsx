import React from 'react';
import { Form } from 'react-bootstrap';
import { LABELS, TIMETOTABLEVALUES } from '../../constants';

type TimeFormProps = {
    onChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
};

const TimeForm = ({ onChange }: TimeFormProps) => {
    return (
        <Form.Control
            as="select"
            onChange={(e: React.ChangeEvent<HTMLInputElement>) => onChange(e)}
            aria-label="time field select a value"
        >
            <option defaultValue={LABELS.TIME}>{LABELS.TIME}</option>
            {TIMETOTABLEVALUES.map((value) => (
                <option
                    key={value.toString()}
                    value={value}
                    aria-label={value.toString() + 'minutes'}
                >
                    {value}
                    {value === 180 ? '+' : ''} min
                </option>
            ))}
        </Form.Control>
    );
};

export default TimeForm;
