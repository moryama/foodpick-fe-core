import { Jumbotron } from 'react-bootstrap';
import styled from 'styled-components';
import { COLORS } from '../../constants';

export const HowItWorksWrapper = styled(Jumbotron)`
    margin-top: 3em;
    background-color: ${COLORS.WHITE};
    border-radius: 10px;
    padding: 2em;
    font-size: 1.3em;
`;
