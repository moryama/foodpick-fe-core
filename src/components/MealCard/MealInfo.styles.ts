import styled from 'styled-components';
import { Row as BSRow } from 'react-bootstrap';

export const Row = styled(BSRow)`
    margin-top: 1.2em;
`;
