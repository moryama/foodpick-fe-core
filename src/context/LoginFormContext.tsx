import React, { useState } from 'react';
import { createContext } from './createContext';

type LoginFormContextType = {
    isLoginFormVisible: boolean;
    setIsLoginFormVisible: React.Dispatch<React.SetStateAction<boolean>>;
};

export const [useLoginFormContext, LoginFormContext] =
    createContext<LoginFormContextType>();
LoginFormContext.displayName = 'LoginFormContext';

export const LoginFormContextProvider = ({
    children,
}: {
    children: React.ReactNode;
}) => {
    const [isLoginFormVisible, setIsLoginFormVisible] =
        useState<boolean>(false);

    return (
        <LoginFormContext.Provider
            value={{ isLoginFormVisible, setIsLoginFormVisible }}
        >
            {children}
        </LoginFormContext.Provider>
    );
};
