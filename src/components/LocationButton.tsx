import { Icon } from './Icon';
import { Location } from '../context/mealFilters/mealFiltersReducer';
import * as S from './LocationButton.styles';

type ButtonType = 'filter' | 'form';

type LocationButtonProps = {
    buttonType: ButtonType;
    isSelected: boolean;
    locationType: Location;
    onClick: (l?: Location) => void;
    value?: Location;
};

type BSButtonSize = 'lg' | 'sm';

const locationButtonSize = {
    form: 'lg',
    filter: 'sm',
};

const locationButtonAriaLabel = {
    action: { form: 'mark as', filter: 'select' },
    location: { home: 'homemade', delivery: 'delivery' },
};

export const LocationButton = ({
    buttonType,
    isSelected,
    locationType,
    onClick,
    value,
}: LocationButtonProps) => {
    return (
        <S.Button
            aria-label={`${locationButtonAriaLabel.action[buttonType]} ${locationButtonAriaLabel.location[locationType]} food`}
            data-test-id={`${locationType}-${buttonType}-button`}
            onClick={() => onClick()}
            size={locationButtonSize[buttonType] as BSButtonSize}
            type="button"
            value={value}
            variant="outline-secondary"
            $isSelected={isSelected}
        >
            {<Icon type={locationType} />}
        </S.Button>
    );
};
