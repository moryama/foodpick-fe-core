import { getErrorMessage } from '../../helpers/getErrorMessage';
import { isInputObjectEmpty } from '../../helpers/isInputObjectEmpty';
import { NotificationData } from './NotificationContext';
import { notifications } from './notifications';

export const getNotificationContent = (notificationData: NotificationData) => {
    let notificationContent;

    const key = notificationData?.label;
    if (notificationData && !isInputObjectEmpty(notificationData)) {
        notificationContent = {
            type: notifications[key].type,
            title: notifications[key].title,
            message: notificationData.error
                ? getErrorMessage(notificationData.error)
                : notifications[key].message,
        };
    }

    return notificationContent;
};
