import { DELIVERY, HOME } from '../../constants';
import { LocationButton } from '../LocationButton';
import { useMealFiltersContext } from '../../context/mealFilters/MealFiltersContext';
import { Location } from '../../context/mealFilters/mealFiltersReducer';

type LocationFilterButtonProps = { isSelected: boolean; type: Location };

const LocationFilterButton = ({
    isSelected,
    type,
}: LocationFilterButtonProps) => {
    const { dispatch } = useMealFiltersContext();

    return (
        <LocationButton
            buttonType="filter"
            data-test-id={`${type}-filter-button`}
            isSelected={isSelected}
            locationType={type}
            onClick={() =>
                dispatch({ type: 'SET_LOCATION_FILTER', value: type })
            }
        />
    );
};

export const LocationFilterButtons = () => {
    const { filters } = useMealFiltersContext();
    const locationFilter = filters.location;

    return (
        <>
            <LocationFilterButton
                isSelected={locationFilter === HOME}
                type={HOME}
            />
            <LocationFilterButton
                isSelected={locationFilter === DELIVERY}
                type={DELIVERY}
            />
        </>
    );
};
