import { Card } from 'react-bootstrap';
import { Ingredients } from '../../types';
import Tag from '../Tag/Tag';
import { v4 as uuidv4 } from 'uuid';

type MealIngredientsProps = {
    ingredients: Ingredients | undefined;
};

export const MealIngredients = ({ ingredients }: MealIngredientsProps) => {
    return (
        <Card.Text>
            {ingredients?.map((ingredient) => (
                <Tag key={uuidv4()} text={ingredient.name} isEnabled={false} />
            ))}
        </Card.Text>
    );
};
