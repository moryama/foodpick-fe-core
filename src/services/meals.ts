import axios from 'axios';
import { MealInputType } from '../components/MealForm/MealForm';
import { IngredientId, MealId, MealType } from '../types';
import { UserToken } from './login';

type GetAllMealsAPIResponse = MealType[];

export type CreateMealAPIResponse = {
    ingredients?: IngredientId[];
    name: string;
    home?: boolean;
    time?: number;
    user: string;
    id: MealId;
};

const baseUrl = '/api/meals';

let token: UserToken;
const setToken = (newToken: UserToken) => {
    token = `bearer ${newToken}`;
};
const clearToken = () => {
    token = '';
};

const getAll = async (): Promise<GetAllMealsAPIResponse> => {
    const config = { headers: { Authorization: token } };
    const response = await axios.get(baseUrl, config);
    return response.data;
};

const create = async (
    newObject: MealInputType
): Promise<CreateMealAPIResponse> => {
    const config = { headers: { Authorization: token } };
    const response = await axios.post(baseUrl, newObject, config);
    return response.data;
};

const drop = async (id: MealId) => {
    const config = { headers: { Authorization: token } };
    await axios.delete(`${baseUrl}/${id}`, config);
    return true;
};

export default { getAll, clearToken, create, drop, setToken };
