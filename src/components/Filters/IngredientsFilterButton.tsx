import { LABELS } from '../../constants';
import * as S from './IngredientsFilterButton.styles';

type IngredientsFilterButtonProps = {
    showIngredients: () => void;
};

export const IngredientsFilterButton = ({
    showIngredients,
}: IngredientsFilterButtonProps) => {
    return (
        <S.Button
            type="button"
            data-test-id="ingredients-filter-button"
            onClick={showIngredients}
            aria-label="filter by ingredients"
        >
            {LABELS.INGREDIENTS}
        </S.Button>
    );
};
