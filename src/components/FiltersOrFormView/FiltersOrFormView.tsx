import FiltersWidget from '../Filters/FiltersWidget';
import MealForm from '../MealForm/MealForm';
import { FiltersWidgetButton } from './FiltersWidgetButton';
import { MealFormButton } from './MealFormButton';
import * as S from './FiltersOrFormView.styles';
import { useToggleVisibility } from '../../hooks/useToggleVisibility';

export const FiltersOrFormView = () => {
    const {
        isFiltersWidgetVisible,
        isMealFormVisible,
        toggleFiltersWidgetVisibility,
        toggleMealFormVisibility,
    } = useToggleVisibility();

    return (
        <>
            <S.ButtonsWrapper>
                <FiltersWidgetButton onClick={toggleFiltersWidgetVisibility} />
                <MealFormButton onClick={toggleMealFormVisibility} />
            </S.ButtonsWrapper>
            <S.FiltersWidgetOrMealFormWrapper>
                {isFiltersWidgetVisible && <FiltersWidget />}
                {isMealFormVisible && <MealForm />}
            </S.FiltersWidgetOrMealFormWrapper>
        </>
    );
};
