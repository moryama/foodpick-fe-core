import styled from 'styled-components';
import { Button as BSButton } from 'react-bootstrap';
import { COLORS } from '../../constants';

export const Button = styled(BSButton)`
    background-color: ${COLORS.FILTER};
    border: ${COLORS.FILTER};
`;
