import { sortArrayAlphabetically } from '../../helpers/sortArrayAlphabetically';
import { Ingredients, IngredientType } from '../../types';
import { IngredientTag } from './IngredientTag';
import * as S from './IngredientsFilterView.styles';

type Props = {
    ingredients: Ingredients;
};

export const IngredientsFilterView = ({ ingredients }: Props) => {
    const ingredientsSortedAlphabetically =
        sortArrayAlphabetically(ingredients);

    return (
        <S.IngredientsFilterViewWrapper>
            {ingredientsSortedAlphabetically.map(
                (ingredient: IngredientType) => (
                    <IngredientTag
                        key={ingredient.id}
                        ingredient={ingredient}
                    />
                )
            )}
        </S.IngredientsFilterViewWrapper>
    );
};
