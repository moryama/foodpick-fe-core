import React, { useReducer } from 'react';
import { createContext } from '../createContext';
import {
    FiltersState,
    FiltersStateAction,
    initialState,
    mealFiltersReducer,
} from './mealFiltersReducer';

export type MealFiltersContextType = {
    filters: FiltersState;
    dispatch: React.Dispatch<FiltersStateAction>;
};

export const [useMealFiltersContext, MealFiltersContext] =
    createContext<MealFiltersContextType>();
MealFiltersContext.displayName = 'MealFiltersContext';

export const MealFiltersContextProvider = ({
    children,
}: {
    children: React.ReactNode;
}) => {
    const [filters, dispatch] = useReducer(mealFiltersReducer, initialState);

    return (
        <MealFiltersContext.Provider value={{ filters, dispatch }}>
            {children}
        </MealFiltersContext.Provider>
    );
};
