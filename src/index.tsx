import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserView, MobileView } from 'react-device-detect';
import App from './App';
import MobileMessage from './components/MobileMessage/MobileMessage';
import { UserContexProvider } from './context/UserContext';
import { NotificationContextProvider } from './context/notification/NotificationContext';

ReactDOM.render(
    <React.StrictMode>
        <BrowserView>
            <UserContexProvider>
                <NotificationContextProvider>
                    <App />
                </NotificationContextProvider>
            </UserContexProvider>
        </BrowserView>
        <MobileView>
            <MobileMessage />
        </MobileView>
    </React.StrictMode>,
    document.getElementById('root')
);
