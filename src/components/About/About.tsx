import DemoButton from './DemoButton';
import HowItWorks from './HowItWorks';
import Catcher from './Catcher';
import * as S from './About.styles';

const About = () => {
    return (
        <S.AboutWrapper>
            <Catcher />
            <DemoButton />
            <HowItWorks />
        </S.AboutWrapper>
    );
};

export default About;
