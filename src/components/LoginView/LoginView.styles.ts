import styled from 'styled-components';

export const LoginViewWrapper = styled.div`
    margin: 5em;
    width: 50%;
    text-align: center;
`;
