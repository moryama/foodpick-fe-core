import { NavDropdown } from 'react-bootstrap';
import * as S from './HowItWorksDropdown.styles';
import { Instructions } from '../Instructions';

export const HowItWorksDropdown = () => {
    return (
        <NavDropdown
            className="dropleft"
            title="How it works"
            id="how-it-works-dropdown"
        >
            <S.DropdownContent>
                <Instructions />
            </S.DropdownContent>
        </NavDropdown>
    );
};
