import { Col } from 'react-bootstrap';
import { Location } from '../../context/mealFilters/mealFiltersReducer';
import { Icon } from '../Icon';

type MealLocationProps = {
    location: Location;
};

export const MealLocation = ({ location }: MealLocationProps) => {
    return (
        <Col>
            <Icon type={location} />
        </Col>
    );
};
