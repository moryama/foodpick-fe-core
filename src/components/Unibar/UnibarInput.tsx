import { ChangeEvent, KeyboardEvent } from 'react';
import { LABELS } from '../../constants';
import { useMealFiltersContext } from '../../context/mealFilters/MealFiltersContext';
import { useMealFormContext } from '../../context/MealFormContext';
import { userPressedEnter } from '../../helpers/userPressedEnter';
import * as S from './UnibarInput.styles';

export const UnibarInput = () => {
    const { filters, dispatch } = useMealFiltersContext();
    const { setIsMealFormVisible } = useMealFormContext();

    const handleOnChange = (event: ChangeEvent<HTMLInputElement>) => {
        const target = event.target as HTMLInputElement;
        dispatch({ type: 'SET_TEXT_FILTER', value: target.value });
    };

    const openMealFormOnEnter = (event: KeyboardEvent<HTMLInputElement>) => {
        if (userPressedEnter(event)) {
            setIsMealFormVisible(true);
        }
    };

    return (
        <S.Input
            data-test-id="unibar"
            value={filters.text}
            onChange={handleOnChange}
            onKeyDown={openMealFormOnEnter}
            aria-label={LABELS.PLACEHOLDERS.UNIBAR}
            autoFocus
        />
    );
};
