import { LABELS } from '../../constants';
import { DemoPanel } from './DemoPanel';
import { LoginForm } from './LoginForm';
import * as S from './LoginView.styles';

export type LoginCredentials = {
    username: string;
    password: string;
};

const LoginView = () => {
    return (
        <S.LoginViewWrapper>
            <DemoPanel />
            <h2>{LABELS.BUTTONS.LOGIN}</h2>
            <LoginForm />
        </S.LoginViewWrapper>
    );
};

export default LoginView;
