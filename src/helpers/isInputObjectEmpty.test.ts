import { isInputObjectEmpty } from './isInputObjectEmpty';

describe(isInputObjectEmpty.name, () => {
    it('should return true id input object is empty', () => {
        const input = {};

        const output = isInputObjectEmpty(input);

        expect(output).toBe(true);
    });

    it('should return false if input object has a key/value', () => {
        const input = { a: '' };

        const output = isInputObjectEmpty(input);

        expect(output).toBe(false);
    });
});
