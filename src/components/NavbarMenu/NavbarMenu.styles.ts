import styled from 'styled-components';
import { Navbar as BSNavbar } from 'react-bootstrap';
import { COLORS } from '../../constants';

export const Navbar = styled(BSNavbar)`
    background-color: ${COLORS.NAVBAR};
    height: 6em;
`;
