import axios from 'axios';
import { IngredientId, Ingredients, IngredientType } from '../types';
const baseUrl = '/api/ingredients';

const getAll = async (): Promise<Ingredients> => {
    const response = await axios.get(`${baseUrl}`);
    return response.data;
};

const getById = async (ingredientId: IngredientId): Promise<IngredientType> => {
    const response = await axios.get(`${baseUrl}/${ingredientId}`);
    return response.data;
};

export default {
    getAll,
    getById,
};
