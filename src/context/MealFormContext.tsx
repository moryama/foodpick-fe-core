import React, { useState } from 'react';
import { createContext } from './createContext';

type MealFormContextType = {
    isMealFormVisible: boolean;
    setIsMealFormVisible: React.Dispatch<React.SetStateAction<boolean>>;
};

export const [useMealFormContext, MealFormContext] =
    createContext<MealFormContextType>();
MealFormContext.displayName = 'MealFormContext';

export const MealFormContextProvider = ({
    children,
}: {
    children: React.ReactNode;
}) => {
    const [isMealFormVisible, setIsMealFormVisible] = useState<boolean>(false);

    return (
        <MealFormContext.Provider
            value={{ isMealFormVisible, setIsMealFormVisible }}
        >
            {children}
        </MealFormContext.Provider>
    );
};
