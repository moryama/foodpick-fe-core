import Tag from '../Tag/Tag';
import { useMealFiltersContext } from '../../context/mealFilters/MealFiltersContext';
import { IngredientType } from '../../types';

type IngredientTagProps = {
    ingredient: IngredientType;
};

export const IngredientTag = ({ ingredient }: IngredientTagProps) => {
    const { filters, dispatch } = useMealFiltersContext();

    return (
        <Tag
            text={ingredient.name}
            isEnabled={true}
            onClick={() =>
                dispatch({
                    type: 'SET_INGREDIENTS_FILTER',
                    value: ingredient.id,
                })
            }
            isSelected={filters.ingredients.includes(ingredient.id)}
        />
    );
};
