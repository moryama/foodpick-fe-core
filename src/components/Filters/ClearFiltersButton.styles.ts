import { Button as BSButton } from 'react-bootstrap';
import styled from 'styled-components';
import { COLORS } from '../../constants';

export const Button = styled(BSButton)`
    text-align: center;
    background-color: ${COLORS.CLEARFILTERS};
    color: ${COLORS.WHITE};
    border: ${COLORS.CLEARFILTERS};
`;
