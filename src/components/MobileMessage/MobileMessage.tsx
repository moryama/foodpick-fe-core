import * as S from './MobileMessage.styles';

const MobileMessage = () => {
    return (
        <S.MobileMessageWrapper>
            <h3>This app is currently unavailable on mobile</h3>
            <h1>🍅</h1>
            <p>Ok...</p>
            <p>
                - take me to the app{' '}
                <a href="https://gitlab.com/moryama/foodpick">source code</a>
            </p>
            <p>
                - take me to the{' '}
                <a href="https://moryama.gitlab.io">developer&apos;s site</a>
            </p>
            <p>
                - show me some{' '}
                <a href="https://duckduckgo.com/?q=italian+regional+dishes&iar=images&iax=images&ia=images">
                    Italian food
                </a>
            </p>
        </S.MobileMessageWrapper>
    );
};

export default MobileMessage;
