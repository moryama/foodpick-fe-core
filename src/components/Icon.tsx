import { GrBike, GrRestaurant, GrClock } from 'react-icons/gr';
import { IconContext } from 'react-icons';
import { LABELS } from '../constants';
import { Location } from '../context/mealFilters/mealFiltersReducer';

type IconType = Location | 'time';

type IconProps = {
    type: IconType;
    size?: string;
};

const icons = {
    home: {
        reactIcon: <GrRestaurant />,
        testId: LABELS.HOME,
    },
    delivery: {
        reactIcon: <GrBike />,
        testId: LABELS.DELIVERY,
    },
    time: {
        reactIcon: <GrClock />,
        testId: LABELS.TIME,
    },
};

const Icon = ({ type, size }: IconProps) => {
    const icon = icons[type];

    return (
        <IconContext.Provider
            value={{ size: size === 'md' ? '1.3em' : '2em', color: 'black' }}
        >
            <span data-test-id={icon.testId}>{icon.reactIcon}</span>
        </IconContext.Provider>
    );
};

export { Icon };
