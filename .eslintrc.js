module.exports = {
    env: {
        browser: true,
        es6: true,
        jest: true,
        node: true,
    },
    extends: [
        'eslint:recommended',
        'plugin:react/recommended',
        'plugin:react-hooks/recommended',
        'plugin:@typescript-eslint/recommended',
    ],
    parser: '@typescript-eslint/parser',
    parserOptions: {
        ecmaFeatures: {
            jsx: true,
        },
        ecmaVersion: 2018,
        sourceType: 'module',
    },
    plugins: ['react', 'jest', 'testing-library', '@typescript-eslint'],
    settings: {
        react: {
            version: 'detect',
        },
    },
    root: true,
    rules: {
        eqeqeq: 'error',
        'no-trailing-spaces': 'error',
        'no-console': 0,
        'react/react-in-jsx-scope': 0,
        'object-curly-spacing': ['error', 'always'],
        'arrow-spacing': ['error', { before: true, after: true }],
        'import/no-anonymous-default-export': 0,
        '@typescript-eslint/explicit-module-boundary-types': 0,
        'linebreak-style': ['error', 'unix'],
        semi: ['error', 'always'],
    },
    overrides: [
        {
            files: ['*.test.ts', '*.test.tsx'],
            rules: {
                '@typescript-eslint/no-explicit-any': ['off'],
            },
        },
    ],
};
