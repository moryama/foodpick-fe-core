import styled from 'styled-components';
import { Spinner as BSSpinner } from 'react-bootstrap';
import { COLORS } from '../../constants';

export const Spinner = styled(BSSpinner)`
    color: ${COLORS.MEAL};
    width: 8em;
    height: 8em;
`;

export const EmptyListWrapper = styled.div`
    text-align: center;
    margin-top: 4em;
`;
