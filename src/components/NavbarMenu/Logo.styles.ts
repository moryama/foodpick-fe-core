import styled from 'styled-components';
import { Navbar as BSNavbar } from 'react-bootstrap';

export const NavbarBrand = styled(BSNavbar.Brand)`
    font-size: 3em;
`;

export const BrandImage = styled.img`
    height: 75px;
`;
