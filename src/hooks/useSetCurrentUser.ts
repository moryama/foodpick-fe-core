import { useEffect } from 'react';
import { useUserContext } from '../context/UserContext';
import mealService from '../services/meals';

export const useSetCurrentUser = () => {
    const { user, dispatch } = useUserContext();

    useEffect(() => {
        const username = localStorage.getItem('username');
        const token = localStorage.getItem('token');

        if (username && token) {
            dispatch({ type: 'SET_USER', user: { username, token } });
            mealService.setToken(token);
        }
    }, [dispatch]);

    return { user };
};
