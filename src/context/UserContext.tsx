import { createContext } from './createContext';
import { LoggedInUser } from '../App';
import React, { useReducer } from 'react';

export type UserContextType = {
    user: LoggedInUser | undefined;
    dispatch: React.Dispatch<UserAction>;
};

export const [useUserContext, UserContext] = createContext<UserContextType>();
UserContext.displayName = 'UserContext';

export type UserAction = {
    type: 'SET_USER' | 'CLEAR_USER';
    user?: LoggedInUser;
};

const initialState = undefined;
const userReducer = (state: LoggedInUser | undefined, action: UserAction) => {
    switch (action.type) {
        case 'SET_USER':
            return action.user;
        case 'CLEAR_USER':
            return initialState;
        default:
            return state;
    }
};

export const UserContexProvider = ({
    children,
}: {
    children: React.ReactNode;
}) => {
    const [user, dispatch] = useReducer(userReducer, initialState);
    return (
        <UserContext.Provider value={{ user, dispatch }}>
            {children}
        </UserContext.Provider>
    );
};
