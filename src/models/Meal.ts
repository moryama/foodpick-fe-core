import { DELIVERY, HOME } from '../constants';
import { IngredientType, MealType } from '../types';

export class Meal {
    ingredients: IngredientType[] | undefined;
    homemade: boolean | undefined;
    time: number | undefined;

    constructor(meal: MealType) {
        this.ingredients = meal?.ingredients;
        this.homemade = meal.home;
        this.time = meal.time;
    }

    get hasIngredients() {
        return this.ingredients && this.ingredients.length > 0;
    }

    get location() {
        const defaultLocation = HOME;

        if (this.homemade === undefined) {
            return defaultLocation;
        }

        return this.homemade ? HOME : DELIVERY;
    }

    // We need to account for existing meals saved with {time: 0} as default
    private get hasTimeZero() {
        return this.time === 0;
    }

    get hasTime() {
        return !this.hasTimeZero && this.time;
    }
}
