import { useEffect, useState } from 'react';
import { LocationFilterButtons } from './LocationFilterButtons';
import { IngredientsFilterButton } from './IngredientsFilterButton';
import { ClearFiltersButton } from './ClearFiltersButton';
import { IngredientsFilterView } from './IngredientsFilterView';
import ingredientsService from '../../services/ingredients';
import * as S from './FiltersWidget.styles';
import { Ingredients } from '../../types';

const FiltersWidget = () => {
    const [isIngredientFilterVisible, setIsIngredientFilterVisible] =
        useState(false);
    const [ingredients, setIngredients] = useState<Ingredients>([]);

    useEffect(() => {
        const initialiseIngredients = async () => {
            try {
                const fetchedIngredients = await ingredientsService.getAll();
                setIngredients(fetchedIngredients);
            } catch (error) {
                throw new Error('I could not fetch ingredients');
            }
        };

        initialiseIngredients();
    }, []);

    const handleIngredientsFilterButton = () => {
        setIsIngredientFilterVisible(!isIngredientFilterVisible);
    };

    return (
        <S.FiltersWidgetWrapper data-test-id="filter-container">
            <LocationFilterButtons />
            <IngredientsFilterButton
                showIngredients={handleIngredientsFilterButton}
            />
            <ClearFiltersButton />
            {isIngredientFilterVisible && (
                <IngredientsFilterView ingredients={ingredients} />
            )}
        </S.FiltersWidgetWrapper>
    );
};

export default FiltersWidget;
