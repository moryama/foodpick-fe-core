import { render, fireEvent, RenderResult } from '@testing-library/react';
import Unibar from './Unibar';

// ISSUE: Jest mock function ====================

// WHAT HAPPENED
// Mock functions worked as long as the function was passed as props to the component
// When the code was changed to define the function directly inside the component mocks stopped working

// TOWARDS A SOLUTION
// We need to find a way to tell Jest that this mock function is connected to our component under test

// ==============================================

describe.skip('<Unibar />', () => {
    let component: RenderResult;
    const mockOnSubmit = jest.fn();

    beforeEach(() => {
        component = render(<Unibar />);
    });

    test('when clicking submit it calls a function to create a new meal with given name', () => {
        const mealName = component.container.querySelector(
            '[data-test-id="unibar"]'
        );
        const form = component.container.querySelector('form');

        mealName &&
            fireEvent.change(mealName, {
                target: { value: 'some meal' },
            });
        form && fireEvent.submit(form);

        expect(mockOnSubmit.mock.calls).toHaveLength(1);

        expect(mockOnSubmit.mock.calls[0][0].name).toBe('some meal');
    });
});
