import styled from 'styled-components';

export const MealsViewWrapper = styled.div`
    display: flex;
    justify-content: space-around;
    flex-wrap: wrap;
    margin-top: 5em;
`;
