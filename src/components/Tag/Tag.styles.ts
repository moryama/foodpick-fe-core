import styled from 'styled-components';
import { COLORS } from '../../constants';

type ButtonProps = {
    readonly isSelected: boolean | undefined;
};

export const Button = styled.button<ButtonProps>`
    border: solid 2px ${COLORS.INGREDIENTS};
    border-radius: 5px;
    margin-top: 0.7em;
    margin-right: 0.5em;
    padding: 0.15em;
    font-size: 0.9em;
    background-color: ${(props) =>
        props.isSelected ? COLORS.SELECTEDFILTER : COLORS.WHITE};
    color: #212529;
`;
