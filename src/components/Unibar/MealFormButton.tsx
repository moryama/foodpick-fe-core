import { LABELS } from '../../constants';
import * as S from './MealFormButton.styles';

type MealFormButtonProps = {
    onClick: () => void;
};

export const MealFormButton = ({ onClick }: MealFormButtonProps) => {
    return (
        <S.Button
            data-test-id="add-button"
            onClick={() => onClick()}
            aria-label="add button"
        >
            {LABELS.BUTTONS.ADD}
        </S.Button>
    );
};
