import { useUserContext } from '../context/UserContext';
import mealService from '../services/meals';

export const useLogout = () => {
    const { dispatch } = useUserContext();

    const logout = () => {
        dispatch({ type: 'CLEAR_USER' });
        localStorage.clear();
        mealService.clearToken();
    };

    return { logout };
};
