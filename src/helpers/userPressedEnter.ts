import { KeyboardEvent } from 'react';

export const userPressedEnter = (event: KeyboardEvent) => event.key === 'Enter';
