export type NotificationLabel =
    | 'LOGIN_SUCCESS'
    | 'LOGIN_FAIL'
    | 'ADD_MEAL_SUCCESS'
    | 'ADD_MEAL_FAIL'
    | 'DELETE_MEAL_SUCCESS'
    | 'DELETE_MEAL_FAIL';

type NotificationType = 'success' | 'warning' | 'danger';

export type NotificationContent = {
    type: NotificationType;
    title: string;
    message: string;
};

type AvailableNotifications = {
    [key in NotificationLabel]: NotificationContent;
};

export const notifications: AvailableNotifications = {
    LOGIN_SUCCESS: {
        type: 'success',
        title: 'Welcome back',
        message: '🖖 🖖 🖖',
    },
    LOGIN_FAIL: { type: 'danger', title: 'Erm...', message: '' },
    ADD_MEAL_SUCCESS: {
        type: 'success',
        title: 'Added!',
        message: '🍔 🍒 🍕 🍵 🍤 🍗 🍝',
    },
    ADD_MEAL_FAIL: {
        type: 'warning',
        title: `Erm... Can't do that`,
        message: '',
    },
    DELETE_MEAL_SUCCESS: {
        type: 'success',
        title: `Woosh... it's gone!`,
        message: '💨 💨 💨',
    },
    DELETE_MEAL_FAIL: {
        type: 'danger',
        title: `Erm...`,
        message: '',
    },
};
