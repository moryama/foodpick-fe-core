import axios from 'axios';
import { LoginCredentials } from '../components/LoginView/LoginView';
const baseUrl = '/api/login';

export type UserToken = string;

export type LoggedInUserAPIResponse = {
    token: UserToken;
    username: string;
};

const login = async (
    credentials: LoginCredentials
): Promise<LoggedInUserAPIResponse> => {
    const response = await axios.post(baseUrl, credentials);
    return response.data;
};

export default { login };
