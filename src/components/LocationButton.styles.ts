import styled from 'styled-components';
import { Button as BSButton } from 'react-bootstrap';
import { COLORS } from '../constants';

export const Button = styled(BSButton)`
    text-align: center;
    background-color: ${(props) =>
        props.$isSelected ? COLORS.SELECTEDFILTER : COLORS.WHITE};
    border: 1px solid #ced4da;
`;
