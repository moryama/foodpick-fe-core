import { LABELS } from '../../constants';
import { useMealsContext } from '../../context/meals/MealsContext';
import { deleteMeal } from '../../context/meals/mealsReducer';
import { useNotificationContext } from '../../context/notification/NotificationContext';
import { MealId } from '../../types';
import * as S from './RemoveButton.styles';

type RemoveButtonProps = {
    mealId: MealId;
    mealName: string;
};

const RemoveButton = ({ mealId, mealName }: RemoveButtonProps) => {
    const { dispatch } = useMealsContext();
    const { setNotification } = useNotificationContext();

    const handleRemove = () => {
        if (window.confirm(`Delete ${mealName}?`)) {
            deleteMeal(dispatch, mealId, setNotification);
        }
    };

    return (
        <S.Button
            data-test-id={`delete-button-${mealName}`}
            onClick={() => handleRemove()}
            aria-label="delete"
        >
            <small className="text-muted">{LABELS.BUTTONS.REMOVE}</small>
        </S.Button>
    );
};

export default RemoveButton;
