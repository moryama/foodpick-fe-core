import styled from 'styled-components';
import { Button as BSButton } from 'react-bootstrap';
import { COLORS } from '../../constants';

export const Button = styled(BSButton)`
    text-align: center;
    background-color: ${COLORS.WHITE};
    color: black;
    border: 1px solid ${COLORS.WHITE};
`;
