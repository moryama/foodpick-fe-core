import { MouseEventHandler } from 'react';
import * as S from './Tag.styles';

type TagProps = {
    text: string;
    isEnabled: boolean;
    onClick?: MouseEventHandler<HTMLButtonElement>;
    isSelected?: boolean;
};

const Tag = ({ text, isEnabled, onClick, isSelected }: TagProps) => {
    return (
        <S.Button
            disabled={!isEnabled}
            onClick={isEnabled ? onClick : undefined}
            isSelected={isSelected}
        >
            {text}
        </S.Button>
    );
};

export default Tag;
