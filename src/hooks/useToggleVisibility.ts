import { useState } from 'react';
import { useMealFormContext } from '../context/MealFormContext';

export const useToggleVisibility = () => {
    const [isFiltersWidgetVisible, setIsFiltersWidgetVisible] = useState(false);
    const { isMealFormVisible, setIsMealFormVisible } = useMealFormContext();

    const toggleMealFormVisibility = () => {
        if (isMealFormVisible) {
            setIsMealFormVisible(false);
        } else {
            setIsMealFormVisible(true);
            setIsFiltersWidgetVisible(false);
        }
    };

    const toggleFiltersWidgetVisibility = () => {
        if (isFiltersWidgetVisible) {
            setIsFiltersWidgetVisible(false);
        } else {
            setIsFiltersWidgetVisible(true);
            setIsMealFormVisible(false);
        }
    };

    return {
        isFiltersWidgetVisible,
        isMealFormVisible,
        setIsMealFormVisible,
        toggleFiltersWidgetVisibility,
        toggleMealFormVisibility,
    };
};
