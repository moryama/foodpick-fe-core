import { MealType } from '../../types';

export const filterMealsByText = (meals: MealType[], text: string) => {
    const filteredMeals = meals.filter((meal) =>
        meal.name.toLowerCase().includes(text.toLowerCase())
    );
    return filteredMeals;
};
