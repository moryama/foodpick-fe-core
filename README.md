[![pipeline status](https://gitlab.com/moryama/foodpick/badges/master/pipeline.svg)](https://gitlab.com/moryama/foodpick/-/commits/master)

# About this repository

This is the migration of [FoodPick FE](https://gitlab.com/moryama/foodpick) from Javascript to Typescript.

The following is a copy of the README from the original project.

# About FoodPick App

A single-page application that lets you log and search ideas for your next meal, solving the problem of food related decision-fatigue.

This is a learning project and a work in progress.

Look at the [backend code](https://gitlab.com/moryama/foodpick-be).

Check out what I'm working on: [current task board](https://www.pivotaltracker.com/n/projects/2616275) 🛶 

Check out the app [demo](https://foodpick-fe-core.onrender.com/) (it's on a free tier, slow loading is expected).

## Content:

-   [Features](#features)
-   [Tools](#tools)
-   [Usage](#usage)
-   [Development notes](#dev-notes)
    -   [End-to-end testing](#testing)
    -   [State management](#state)

## Features <a name="features"></a>

-   card visualization
-   interface to quickly add, filter and remove cards
-   multi-filter search
-   unified usage of icons across functionalities
-   notification system
-   token authentication system
-   component unit tests and e2e tests
-   CI/CD pipelines

## Tools <a name="tools"></a>

-   [React 17.0.1](https://reactjs.org/)

-   Bootstrapped with [Create React App](https://github.com/facebook/create-react-app)

-   Authentication: [JWT](https://jwt.io/), [Bcrypt](https://www.npmjs.com/package/bcrypt)

-   Testing: [Jest](https://jestjs.io/docs/en/getting-started), [Testing-Library](https://testing-library.com/docs/react-testing-library/api/), [Cypress](https://docs.cypress.io/guides/overview/why-cypress.html#In-a-nutshell)

-   Style: [react-bootstrap](https://react-bootstrap.github.io/)

-   Deployed on: [Render](https://www.render.com/)

## Usage <a name="usage"></a>

### **Run React app**

```
$ npm install
$ npm start
```

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### **Run Cypress tests**

```
$ npm start  // terminal 1

// In Foodpick_BE run the backend server

$ npm run start:test // terminal 3
$ npm run cypress:open // terminal 2
```

### **Run Jest tests**

```
$ npm test
```

## Credits

Logo by [Muhammad Haq](https://freeicons.io/profile/823) on [freeicons.io](https://freeicons.io).

## License

No license is available at the moment.

# Development notes <a name="dev-notes"></a>

### **End-to-end testing** <a name="testing"></a>

The project features tests of the user-experience implemented with Cypress.

Cypress great API and documentation make frontend testing very pleasant and easy-going.

> **UPDATE:** Cypress tests are waiting to be moved to their own repository. In the meantime they were moved to the backend [repository](https://gitlab.com/moryama/foodpick-be) of this project to serve in the CI/CD pipeline.

**ELEMENT SELECTION**

I followed two approaches for selecting elements during testing.

One is to identify the element **by its visible content**.

This approach reflects how an actual user would find these elements on the page and works well with buttons and input fields:

```
cy.contains(LABELS.HOME) // what the user sees and clicks: 🏠
  .click();
```

> **UPDATE:** There was a change in the application tests exactly in the line above.
> Now the test uses a **custom id** (as shown below) to select the button.
> The reason is that the `LABELS.HOME` button symbol 🏠 is now replaced by an icon component passed with the `react-icon` library. I still would like to have this test select the element by its visible content, so I'm looking for a solution.

When this is not feasible - like in the case of a `placeholder` content which is not natively a selector in Cypress - I decided to identify elements with **custom ids** that are specific for tests:

```
// Element definition
<input
  data-test-id='ingredients'  // define custom id
/>

// Cypress test
cy.get('[data-test-id=ingredients]')  // use custom id
  .type('ingredient')
```

This **isolates the selectors** from future CSS or JS changes, as reminded by Cypress [best practices](https://docs.cypress.io/guides/references/best-practices.html#Selecting-Elements) for selecting elements.

### **State management** <a name="state"></a>

The original project used a combination of local state and Redux to handle any state shared across components.

In this project version, Redux has been replaced completely by a pattern of `React.Context` combined with `useReducer`.

As a rule of thumb:

the app uses **React.Context** for any state that needs to be shared across the application, such as

-   the state of togglable elements that influence the rendering of content in other components
-   the state of a multi-field filter is set inside a Provider so any component can update it and access its current values

the app stores **directly in the component** any state that is only used once and locally, such as

-   the state of togglable elements that have effects only within their component
-   the current state of the login input fields
