import styled from 'styled-components';
import { COLORS } from '../../constants';

export const ContentWrapper = styled.div`
    background-color: ${COLORS.DEMO};
    padding: 1em;
    margin-bottom: 1em;
    border-radius: 5px;
`;
