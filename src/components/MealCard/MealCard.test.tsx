import '@testing-library/jest-dom/extend-expect';
import { render } from '@testing-library/react';
import { ContextProviders } from '../../tests/ContextProviders';
import { MealType } from '../../types';
import MealCard from './MealCard';

const getMealComponent = (data: MealType) => {
    return render(
        <ContextProviders>
            <MealCard meal={data} />
        </ContextProviders>
    );
};

describe('<Meal />', () => {
    test('should render meal info', () => {
        const meal = {
            name: 'meal 1',
            time: 10,
            user: 'user',
            id: 'id',
        };

        const component = getMealComponent(meal);

        expect(component.container).toHaveTextContent('meal 1');
        expect(component.container).toHaveTextContent('10 min');
    });

    test('should render remove button', () => {
        const meal = {
            name: 'meal 1',
            user: 'user',
            id: 'id',
        };

        const component = getMealComponent(meal);

        const removeButton = component.getByRole('button', { name: /delete/i });
        expect(removeButton).toBeInTheDocument();
    });

    test('does not render time if is 0', () => {
        const meal = {
            name: 'meal 1',
            time: 0,
            user: 'user',
            id: 'id',
        };

        const component = getMealComponent(meal);
        const time = component.queryByText('0');

        expect(time).not.toBeInTheDocument();
    });
});
