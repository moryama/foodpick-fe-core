import { useMealFilters } from '../../hooks/useMealFilters';
import { useMealsContext } from '../../context/meals/MealsContext';
import MealsList from './MealsList';
import * as S from './MealsView.styles';

const MealsView = () => {
    const { meals, isLoading } = useMealsContext();
    const allMeals = meals;

    const filteredMeals = useMealFilters(allMeals);
    const mealsToShow = filteredMeals || allMeals;

    return (
        <S.MealsViewWrapper data-test-id="meal-list">
            <MealsList meals={mealsToShow} isLoading={isLoading} />
        </S.MealsViewWrapper>
    );
};

export default MealsView;
