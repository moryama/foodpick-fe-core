import { LABELS } from '../../constants';
import * as S from './SaveButton.styles';

type SaveButtonProps = { isEnabled: boolean };

const SaveButton = ({ isEnabled }: SaveButtonProps) => {
    return (
        <S.Button
            data-test-id="add-button"
            type="submit"
            aria-label="save button"
            disabled={!isEnabled}
        >
            {LABELS.BUTTONS.SAVE}
        </S.Button>
    );
};

export default SaveButton;
