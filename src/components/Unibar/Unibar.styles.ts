import styled from 'styled-components';

export const UnibarWrapper = styled.div`
    margin-top: 5em;
    text-align: center;
    width: 100%;
`;

export const UnibarLabel = styled.label`
    font-size: large;
`;
