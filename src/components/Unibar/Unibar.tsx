import { LABELS } from '../../constants';
import { UnibarInput } from './UnibarInput';
import * as S from './Unibar.styles';
import { FiltersOrFormView } from '../FiltersOrFormView/FiltersOrFormView';

const Unibar = () => {
    return (
        <S.UnibarWrapper>
            <S.UnibarLabel>{LABELS.PLACEHOLDERS.UNIBAR}</S.UnibarLabel>
            <UnibarInput />
            <FiltersOrFormView />
        </S.UnibarWrapper>
    );
};

export default Unibar;
