import { LABELS } from '../../constants';
import { useLoginFormContext } from '../../context/LoginFormContext';
import * as S from './DemoButton.styles';

const DemoButton = () => {
    const { setIsLoginFormVisible } = useLoginFormContext();

    return (
        <S.DemoButtonWrapper>
            <S.Button size="lg" onClick={() => setIsLoginFormVisible(true)}>
                {LABELS.BUTTONS.DEMO}
            </S.Button>
        </S.DemoButtonWrapper>
    );
};

export default DemoButton;
