import styled from 'styled-components';

export const ButtonsWrapper = styled.div`
    margin: 1em;
`;

export const FiltersWidgetOrMealFormWrapper = styled.div`
    margin: 1em;
`;
