import { IngredientId, MealType } from '../../types';
import { filterMealsByIngredients } from './filterMealsByIngredient';

describe('filterMealsByIngredient', () => {
    it('should return a meal IF it has both ingredients', () => {
        const inputMeals: MealType[] = [
            {
                ingredients: [
                    { id: '1', name: 'one' },
                    { id: '2', name: 'two' },
                ],
                name: 'meal 1',
                user: 'user',
                id: '1',
            },
        ];
        const inputIngredients: IngredientId[] = ['1', '2'];
        const expectedOutput: MealType[] = [
            {
                ingredients: [
                    { id: '1', name: 'one' },
                    { id: '2', name: 'two' },
                ],
                name: 'meal 1',
                user: 'user',
                id: '1',
            },
        ];

        const output = filterMealsByIngredients(inputMeals, inputIngredients);

        expect(output).toEqual(expectedOutput);
    });

    it('should return a meal IF it has both ingredients WHEN there are multiple meals', () => {
        const inputMeals: MealType[] = [
            {
                ingredients: [
                    { id: '1', name: 'one' },
                    { id: '2', name: 'two' },
                ],
                name: 'meal 1',
                user: 'user',
                id: '1',
            },
            {
                ingredients: [{ id: '2', name: 'two' }],
                name: 'meal 2',
                user: 'user',
                id: '2',
            },
        ];
        const inputIngredients: IngredientId[] = ['1', '2'];
        const expectedOutput: MealType[] = [
            {
                ingredients: [
                    { id: '1', name: 'one' },
                    { id: '2', name: 'two' },
                ],
                name: 'meal 1',
                user: 'user',
                id: '1',
            },
        ];

        const output = filterMealsByIngredients(inputMeals, inputIngredients);

        expect(output).toEqual(expectedOutput);
    });

    it('should NOT return a meal IF it has only one of two ingredients', () => {
        const inputMeals: MealType[] = [
            {
                ingredients: [{ id: '1', name: 'one' }],
                name: 'meal 1',
                user: 'user',
                id: '1',
            },
        ];
        const inputIngredients: IngredientId[] = ['1', '2'];
        const expectedOutput: MealType[] = [];

        const output = filterMealsByIngredients(inputMeals, inputIngredients);

        expect(output).toEqual(expectedOutput);
    });

    it('should NOT return a meal IF it has only one of two ingredients WHEN there are multiple meals', () => {
        const inputMeals: MealType[] = [
            {
                ingredients: [{ id: '1', name: 'one' }],
                name: 'meal 1',
                user: 'user',
                id: '1',
            },
            {
                ingredients: [{ id: '2', name: 'two' }],
                name: 'meal 2',
                user: 'user',
                id: '2',
            },
        ];
        const inputIngredients: IngredientId[] = ['1', '2'];
        const expectedOutput: MealType[] = [];

        const output = filterMealsByIngredients(inputMeals, inputIngredients);

        expect(output).toEqual(expectedOutput);
    });
});
