import styled from 'styled-components';

export const Input = styled.input`
    width: 80%;
    height: 2.3em;
    border-radius: 4px;
    border: solid 1px black;
    text-align: center;
`;
