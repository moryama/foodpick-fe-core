import { Col } from 'react-bootstrap';
import { LABELS } from '../../constants';
import { Icon } from '../Icon';
import { MealType } from '../../types';
import * as S from './MealInfo.styles';
import { Meal } from '../../models/Meal';
import { MealLocation } from './MealLocation';
import { MealIngredients } from './MealIngredients';

type MealInfoProps = {
    meal: MealType;
};

export const MealInfo = ({ meal }: MealInfoProps) => {
    const mealObject = new Meal(meal);

    return (
        <>
            <S.Row>
                <Col>{<MealLocation location={mealObject.location} />}</Col>
                <Col>
                    {mealObject.hasTime && (
                        <>
                            <Icon type="time" /> {meal.time} {LABELS.TIMESPEC}
                        </>
                    )}
                </Col>
            </S.Row>
            {mealObject.hasIngredients && (
                <MealIngredients ingredients={meal.ingredients} />
            )}
        </>
    );
};
