// The type of `error` will be known only at runtime so TS will never have a way to know about it

import { UNKNOWN_ERROR_MESSAGE } from '../constants';

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const getErrorMessage = (error: any) => {
    let errorMessage;
    if (error.isAxiosError) {
        errorMessage = error.response.data.error;
    } else {
        errorMessage = UNKNOWN_ERROR_MESSAGE;
    }
    return errorMessage;
};
