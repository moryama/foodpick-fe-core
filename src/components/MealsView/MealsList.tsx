import MealCard from '../MealCard/MealCard';
import { MealType } from '../../types';
import { LABELS } from '../../constants';
import * as S from './MealsList.styles';

type MealsListProps = {
    meals: MealType[];
    isLoading: boolean;
};

const MealsList = ({ meals, isLoading }: MealsListProps) => {
    if (isLoading) {
        return <S.Spinner animation="border" role="status" />;
    }

    if (meals.length === 0) {
        return (
            <S.EmptyListWrapper>
                <h2>{LABELS.EMPTYMEALLIST}</h2>
            </S.EmptyListWrapper>
        );
    }

    return (
        <>
            {meals.map((meal) => (
                <MealCard key={meal.id} meal={meal} />
            ))}
        </>
    );
};

export default MealsList;
