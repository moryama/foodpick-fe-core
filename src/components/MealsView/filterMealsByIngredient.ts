import { IngredientId, MealType } from '../../types';

const getMealIngredientsIds = (meal: MealType) => {
    return meal.ingredients?.map((item) => item.id);
};

const hasMealAllSelectedIngredients = (
    meal: MealType,
    selectedIngredientsIds: IngredientId[]
) => {
    const mealIngredientsIds = getMealIngredientsIds(meal);

    return selectedIngredientsIds.every((item) =>
        mealIngredientsIds?.includes(item)
    );
};

export const filterMealsByIngredients = (
    meals: MealType[],
    selectedIngredients: IngredientId[]
): MealType[] => {
    const filteredMeals: MealType[] = [];

    meals.forEach((meal) => {
        if (hasMealAllSelectedIngredients(meal, selectedIngredients)) {
            filteredMeals.push(meal);
        }
    });

    return filteredMeals;
};
