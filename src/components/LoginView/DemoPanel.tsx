import * as S from './DemoPanel.styles';

export const DemoPanel = () => {
    return (
        <>
            <S.ContentWrapper>
                <h3>Here&apos;s your demo account:</h3>
                <h4>
                    username: <b>demo</b>
                </h4>
                <h4>
                    password: <b>demo</b>
                </h4>
            </S.ContentWrapper>
            <S.ContentWrapper>
                <h5>
                    ⚠️ Once a day the data on the app is deleted and reset. Feel
                    free to play around 🍻
                </h5>
            </S.ContentWrapper>
        </>
    );
};
